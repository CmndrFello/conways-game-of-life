#pragma once
#include <iostream>
#include <vector>
#include "cell.h"

class Grid
{
	Cell *_cells;
	int _width;
	int _height;

	int _getPos(const int x, const int y);

public:
	Grid(const int width, const int height);
	~Grid();

	int getWidth() { return _width; }
	int getHeight() { return _height; }

	bool isValidCell(const int x, const int y);
	Cell& getCell(const int x, const int y);
	bool setCellState(const int x, const int y, bool new_state);
};