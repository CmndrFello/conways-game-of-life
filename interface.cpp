#include "interface.h"

Interface::Interface(Conway &conway, sf::Vector2i screen_size) : _conway(conway), _screen_size(screen_size)
{
	_camera.setPos(sf::Vector2f(_conway.getGrid()->getWidth() / 2.f, _conway.getGrid()->getHeight() / 2.f));
	_window.create(sf::VideoMode(_screen_size.x, _screen_size.y), WINDOW_TITLE, sf::Style::Titlebar | sf::Style::Close);

	// Load font
	_font = sf::Font{};
	if (!_font.loadFromFile("font.ttf")) {
		// Error while loading font
		throw _exception();
	};
	_font.setSmooth(false);

	// Load window icon
	auto icon = sf::Image{};
	if (!icon.loadFromFile("icon.png")) {
		// Error while loading icon
		throw _exception();
	}

	// Set window icon
	_window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
}

Interface::~Interface()
{

}

sf::Vector2i Interface::getScreenSize()
{
	return _screen_size;
}

int Interface::getScreenWidth()
{
	return _screen_size.x;
}

int Interface::getScreenHeight()
{
	return _screen_size.y;
}

int Interface::getHScreenWidth()
{
	return _screen_size.x / 2;
}

int Interface::getHScreenHeight()
{
	return _screen_size.y / 2;
}

float Interface::screenCellRatio()
{
	return CELL_SIZE * _camera.getZoom();
}

sf::Vector2f Interface::localToScreen(sf::Vector2i grid_pos, sf::Vector2f local_pos)
{
	sf::Vector2f screen_pos;
	float ratio = screenCellRatio();
	screen_pos.x = getHScreenWidth() + (grid_pos.x - local_pos.x) * ratio;
	screen_pos.y = getHScreenHeight() + (grid_pos.y - local_pos.y) * ratio;
	return screen_pos;
}

sf::Vector2f Interface::screenToLocal(sf::Vector2f screen_pos, sf::Vector2f local_pos)
{
	sf::Vector2f grid_pos;
	float ratio = screenCellRatio();
	grid_pos.x = local_pos.x + (screen_pos.x - getHScreenWidth()) / ratio;
	grid_pos.y = local_pos.y + (screen_pos.y - getHScreenHeight()) / ratio;
	return grid_pos;
}

sf::Vector2f Interface::gridToScreen(sf::Vector2i grid_pos)
{
	return localToScreen(grid_pos, _camera.getPos());
}

sf::Vector2f Interface::screenToGrid(sf::Vector2f screen_pos)
{
	return screenToLocal(screen_pos, _camera.getPos());
}

void Interface::addConsoleLine(std::string text)
{
	std::string upper_text = text;
	std::transform(upper_text.begin(), upper_text.end(), upper_text.begin(), ::toupper);
	ConsoleLine new_line{ upper_text, CONSOLE_LOG_TIME };
	_console_deque.push_front(new_line);
}

void Interface::updateConsoleLines(float dt)
{
	for (auto it = _console_deque.begin(); it != _console_deque.end(); it++) { 
		it->time -= dt;
		it->ratio = it->time / CONSOLE_LOG_TIME;
	}

	if (!_console_deque.empty()) {
		if (_console_deque.back().time <= 0) _console_deque.pop_back();
	}
}

void Interface::end()
{
	// End program
	_window.close();
}

void Interface::run()
{
	sf::Event event;

	while (_window.isOpen())
	{
		// Get current grid
		Grid* grid = _conway.getGrid();

		// Get delta time
		float dt = _deltaClock.restart().asSeconds();

		// Framerate deque
		// Push back 'fps_sample_num' amount of delta time values and calculate sum into 'fps' variable
		_deltaDeque.push_back(dt);
		if (_deltaDeque.size() >= FPS_SAMPLE_NUM) {
			for (auto dta : _deltaDeque) _frames_per_second += 1 / dta;
			_frames_per_second /= FPS_SAMPLE_NUM;
			_deltaDeque.clear();
		}

		while (_window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				// Window prompted to close
				end();
			}

			else if (event.type == sf::Event::KeyPressed)
			{
				// When user presses the 'escape' key, end program
				if (event.key.scancode == sf::Keyboard::Scan::Escape) {
					end();
				}

				// When user presses the 'space' key, the simulation will pause
				if (event.key.scancode == sf::Keyboard::Scan::Space) {
					_paused = !_paused;
					addConsoleLine(_paused ? "Simulation Is Paused" : "Resumed Simulation");
				}

				// When user presses the 's' key, the simulation is stepped once
				if (event.key.scancode == sf::Keyboard::Scan::S && _paused) {
					_conway.step();
					addConsoleLine("Simulation Stepped Forward");
				}

				// When user presses the right or left, the speed increases / decreases
				if (event.key.scancode == sf::Keyboard::Scan::Right) {
					_speed *= 2;
					addConsoleLine("Speed Up");
				}
				else if (event.key.scancode == sf::Keyboard::Scan::Left) {
					_speed *= .5f;
					addConsoleLine("Speed Down");
				}
				if (_speed < 0) { _speed = 0; }

				// When user presses '+' or '-', the zoom increases / decreases
				if (event.key.scancode == sf::Keyboard::Scan::NumpadPlus) {
					_camera.zoomIn();
				}
				if (event.key.scancode == sf::Keyboard::Scan::NumpadMinus) {
					_camera.zoomOut();
				}
			}

			else if (event.type == sf::Event::MouseWheelScrolled)
			{
				if (!_mouse_panning && event.mouseWheelScroll.wheel == sf::Mouse::VerticalWheel) {
					// Zoom in & out depending on scroll wheel direction
					if (event.mouseWheelScroll.delta > 0) {
						_camera.zoomIn();
					}
					else {
						_camera.zoomOut();
					}

					// Not accurate, mouse needs to stay on current cell
					_camera.setPos((sf::Vector2f)_mouse_on_grid);
				}
			}

			else if (event.type == sf::Event::MouseMoved)
			{
				// Get current mouse pixel position relative to the top-left of the screen
				_mouse_on_screen.x = (float)event.mouseMove.x;
				_mouse_on_screen.y = (float)event.mouseMove.y;

				// And get the cell position in the grid
				_mouse_on_grid = (sf::Vector2i)screenToGrid(_mouse_on_screen);

				// Clip values inside of grid
				_mouse_on_grid.x = clip(_mouse_on_grid.x, 0, _conway.getGrid()->getWidth() - 1);
				_mouse_on_grid.y = clip(_mouse_on_grid.y, 0, _conway.getGrid()->getHeight() - 1);
			}

			else if (event.type == sf::Event::MouseButtonPressed)
			{
				// Get cell under mouse
				Cell cell = grid->getCell(_mouse_on_grid.x, _mouse_on_grid.y);

				// Change state of cell depending on mouse click
				if (event.mouseButton.button == sf::Mouse::Left) {
					grid->setCellState(_mouse_on_grid.x, _mouse_on_grid.y, !cell.isAlive());
					addConsoleLine("Cell Update");
				}

				// Pan camera with right click
				if (event.mouseButton.button == sf::Mouse::Right) {
					// On right mouse-button hold
					if (!_mouse_panning) {
						_mouse_panning_start = _mouse_on_screen;
						_mouse_panning = true;
					}
				}
			}

			else if (event.type == sf::Event::MouseButtonReleased)
			{
				// On right mouse-button released
				if (event.mouseButton.button == sf::Mouse::Right) {
					_camera.setPos(_render_pos);
					_mouse_panning_start = sf::Vector2f(0, 0);
					_mouse_panning = false;
				}
			}
		}

		// Clear and start rendering content

		// When simulation is paused, fill with blue, otherwise black
		sf::Color background = (_paused) ? COLOR_BG_PAUSED : COLOR_BG;
		_window.clear(background);

		_render_pos = _camera.getPos();

		// Mouse panning offset
		if (_mouse_panning) {
			sf::Vector2f offset = _mouse_on_screen - _mouse_panning_start;
			_render_pos = _camera.getPos() - offset / screenCellRatio();
		}

		// Draw grid independently of simulation tick		
		sf::Vector2i top_left = (sf::Vector2i)screenToLocal(sf::Vector2f(0.f,0.f), _render_pos);
		sf::Vector2i bottom_right = (sf::Vector2i)screenToLocal(sf::Vector2f((float)getScreenWidth(), (float)getScreenHeight()), _render_pos);

		// Clamp values for only on grid
		top_left.x = clip(top_left.x, 0, colmax);
		top_left.y = clip(top_left.y, 0, rowmax);
		bottom_right.x = clip(bottom_right.x, 0, colmax);
		bottom_right.y = clip(bottom_right.y, 0, rowmax);

		int view_width = bottom_right.x - top_left.x;
		int view_height = bottom_right.y - top_left.y;

		// Iterate over cell range and render
		sf::Image image;
		image.create(view_width, view_height, sf::Color::Transparent);
		for (int y = 0; y < view_height; y++) {
			for (int x = 0; x < view_width; x++) {
				Cell cell = grid->getCell(top_left.x + x, top_left.y + y);
				if (cell.isAlive()) image.setPixel(x, y, COLOR_CELL);	
			}
		}
		// Convert image data into texture
		sf::Texture view_texture;
		view_texture.loadFromImage(image);
		// Create sprite and set view texture to it
		sf::Sprite view_sprite;
		view_sprite.setTexture(view_texture);
		view_sprite.setScale(sf::Vector2f(1, 1) * screenCellRatio());
		view_sprite.setPosition(localToScreen(top_left, _render_pos));
		// Draw sprite with view image on screen
		_window.draw(view_sprite);
		
		// Draw mouse grid cursor
		sf::RectangleShape cursor(sf::Vector2f(screenCellRatio(), screenCellRatio()));
		cursor.setFillColor(sf::Color(0, 255, 0, 100));
		cursor.setPosition(gridToScreen(_mouse_on_grid));
		_window.draw(cursor);

		// Draw debug text for user
		sf::RectangleShape debug_rect(sf::Vector2f((float)getScreenWidth(), 18));
		debug_rect.setFillColor(_paused ? sf::Color::Red : sf::Color::Blue);

		_window.draw(debug_rect);
		
		sf::Text debug_text(
			"zoom=" + std::to_string(round(_camera.getZoom() * 100) / 100) +
			" speed=" + std::to_string(_speed) +
			" step=" + std::to_string(_conway.getStepCount()) +
			" fps=" + std::to_string(_frames_per_second),
			_font,
			16
		);
		debug_text.setPosition(sf::Vector2f(0, 2));

		_window.draw(debug_text);

		// Draw console lines
		updateConsoleLines(dt); // Update time values
		
		for (int i = 0; i < _console_deque.size(); i++) {
			ConsoleLine& line = _console_deque[i];

			sf::Text console_text(line.text, _font, 16);

			console_text.setFillColor(
				sf::Color(255, 255, 255, (sf::Uint8)(255 * line.ratio))
			);
			console_text.setPosition(sf::Vector2f(0.f, float(getScreenHeight() - 22 * (i+1))));

			_window.draw(console_text);
		}

		_window.display();

		// Simulation tick initiated every 'tps',
		// - tbt stores time between ticks
		if (!_paused)
		{
			_tbt += dt;

			if (_tbt >= (TPS / _speed))
			{
				_conway.step();
				_tbt = 0;
			}
		}
	}
}