#pragma once
#include "life.h"

class Cell
{
	bool _state = STATE_DEAD;

public:
	Cell() {};

	bool isDead() { return !_state; };
	bool isAlive() { return _state; };

	void setState(bool new_state) { _state = new_state; }
};