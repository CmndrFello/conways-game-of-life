#include "conway.h"

Conway::Conway()
{
	_grid = new Grid{ colmax, rowmax };

	for (int row = 1; row < _grid->getHeight() - borderwidth; row++)
	{
		for (int col = 1; col < _grid->getWidth() - borderwidth; col++)
		{
			// Randomly set cell state
			double state = ((double)rand()) / RAND_MAX;

			if (round(state) == 1) {
				_grid->setCellState(col, row, STATE_ALIVE);
			}
			else {
				_grid->setCellState(col, row, STATE_DEAD);
			}
		}
	}
}

Conway::~Conway()
{
	delete _grid;
}

int Conway::_countNeighboursOfCell(const int x, const int y)
{
	int neighbours = 0;

	for (int cy = y - 1; cy <= y + 1; cy++) {
		for (int cx = x - 1; cx <= x + 1; cx++) {
			if (cx == x && cy == y) continue; // Skip the actual cell

			Cell ccell = _grid->getCell(cx, cy);
			if (ccell.isAlive()) { neighbours++; }
		}
	}

	return neighbours;
}

bool Conway::_canSurvive(const int x, const int y)
{
	int neighbours = _countNeighboursOfCell(x, y);

	if (neighbours < min_neighbours || neighbours > max_neighbours) return false; // This cell cannot survive
	return true; // Has enough neighbours to stay alive
}

bool Conway::_canSpawn(const int x, const int y)
{
	int neighbours = _countNeighboursOfCell(x, y);

	if (neighbours < min_parents || neighbours > max_neighbours) return false; // This cell cannot be born
	return true; // Has enough parents to be born
}

void Conway::step()
{
	Grid *new_grid = new Grid{ colmax, rowmax };;

	for (int row = 1; row < _grid->getHeight() - borderwidth; row++)
	{
		for (int col = 1; col < _grid->getWidth() - borderwidth; col++)
		{
			Cell cell = _grid->getCell(col, row);

			new_grid->setCellState(col, row, cell.isAlive());

			// Spawn or kill cells according to rules
			if (cell.isAlive())
			{
				if (!_canSurvive(col, row))
				{
					new_grid->setCellState(col, row, STATE_DEAD);
				}
			}
			else
			{
				if (_canSpawn(col, row))
				{
					new_grid->setCellState(col, row, STATE_ALIVE);

				}
			}
		}
	}

	_grid = new_grid;
	_steps++;
}

int Conway::getStepCount()
{
	return _steps;
}

Grid* Conway::getGrid()
{
	return _grid;
}