#pragma once
#include "SFML/Graphics/Color.hpp"

template <typename T>
T clip(const T& n, const T& lower, const T& upper) {
	return std::max(lower, std::min(n, upper));
}

const sf::Color COLOR_CELL = sf::Color::White;
const sf::Color COLOR_BG = sf::Color::Black;
const sf::Color COLOR_BG_PAUSED = sf::Color::Blue;
const sf::Color COLOR_CURSOR = sf::Color::Green;

const float TPS = 0.2f; // Time per step
const int CELL_SIZE = 5; // pixels
const int CURSOR_THICKNESS = 2;
const int FPS_SAMPLE_NUM = 100;