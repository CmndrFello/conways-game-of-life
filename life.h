#pragma once

#define STATE_DEAD false
#define STATE_ALIVE true

const char live_cell{ 'X' };
const char dead_cell{ ' ' };

// Standarad ANSI console, with 1-char border
// Occupied rows go from 1 to 23
// Occupied columns go from 1 to 79
const int rowmax = 120;
const int colmax = 160;
const int borderwidth = 1;

// Conway's parameters
const int min_neighbours = 2;
const int max_neighbours = 3;
const int min_parents = 3;
const int max_parents = 3;