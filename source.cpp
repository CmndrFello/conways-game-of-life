#include <iostream>
#include "interface.h"
#include "conway.h"

int main(void)
{
	Conway simulation;
	Interface main_interface(simulation, sf::Vector2i(800,600));
	main_interface.run();

	return 0;
}