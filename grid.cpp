#include "grid.h"

Grid::Grid(const int width, const int height) : _width(width), _height(height) {
	_cells = new Cell[width * height]();
};

Grid::~Grid() {
	delete[] _cells;
}

int Grid::_getPos(const int x, const int y) { return (y * _width) + x; };

bool Grid::isValidCell(const int x, const int y) {
	return !((x < 0 || x > _width) || (y < 0 || y > _height));
}
Cell& Grid::getCell(const int x, const int y) {
	return _cells[_getPos(x, y)];
}

bool Grid::setCellState(const int x, const int y, bool new_state) {
	if (isValidCell(x, y)) {
		getCell(x, y).setState(new_state);
		return true;
	}
	return false;
}