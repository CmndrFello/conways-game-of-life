#include <iostream>
#include <deque>
#include <fstream>
#include <algorithm>

#include "SFML/Graphics.hpp"

#include "const.h"
#include "conway.h"
#include "camera.h"

const std::string WINDOW_TITLE = "Conway's Game of Life";
const float CONSOLE_LOG_TIME = 3.f;

struct ConsoleLine
{
	std::string text;
	float time = CONSOLE_LOG_TIME;
	float ratio = 1;
};

class Interface
{
	Conway &_conway;
	sf::RenderWindow _window;
	Camera _camera;

	sf::Vector2i _screen_size;
	sf::Font _font;

	sf::Clock _deltaClock;
	std::deque<float> _deltaDeque;
	float _frames_per_second = 0;

	sf::Vector2f _render_pos;				// Actual render position
	
	float _tbt = 0.f;						// Accumulator of time between ticks
	bool _paused = false;					// Simulation paused variable
	float _speed = 1.f;						// Simulation speed variable
	
	sf::Vector2i _mouse_on_grid;			// Mouse relative to grid
	sf::Vector2f _mouse_on_screen;			// Mouse relative to window screen
	bool _mouse_panning = false;			// Is mouse panning
	sf::Vector2f _mouse_panning_start;		// Origin of mouse panning

	std::deque<ConsoleLine> _console_deque;	// Stack on console lines

public:
	Interface(Conway &conway, sf::Vector2i screen_size);
	~Interface();

	// Getters
	sf::Vector2i getScreenSize();
	int getScreenWidth();
	int getScreenHeight();
	int getHScreenWidth();
	int getHScreenHeight();

	// Functions
	float screenCellRatio();
	sf::Vector2f localToScreen(sf::Vector2i grid_pos, sf::Vector2f local_pos);
	sf::Vector2f screenToLocal(sf::Vector2f screen_pos, sf::Vector2f local_pos);

	sf::Vector2f gridToScreen(sf::Vector2i grid_pos);
	sf::Vector2f screenToGrid(sf::Vector2f screen_pos);

	void addConsoleLine(std::string text);
	void updateConsoleLines(float dt);

	void end();
	void run();
};