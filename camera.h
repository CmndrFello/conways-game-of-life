#pragma once
#include <SFML/Graphics.hpp>

class Camera
{
	sf::Vector2f _pos;
	float _zoom;

public:
	Camera();

	// Getters
	sf::Vector2f getPos();
	float getZoom();
	
	// Setters
	void setPos(sf::Vector2f new_pos);

	// Functions
	void zoomIn();
	void zoomOut();
};