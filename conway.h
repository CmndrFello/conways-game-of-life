#pragma once
#include <vector>
#include <math.h>

#include "life.h"
#include "grid.h"

class Conway
{
	int _steps = 0;
	Grid *_grid;

	int _countNeighboursOfCell(const int x, const int y);
	bool _canSurvive(const int x, const int y);
	bool _canSpawn(const int x, const int y);

public:
	Conway();
	~Conway();

	void step();
	int getStepCount();
	Grid* getGrid();
};