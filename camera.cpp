#include "camera.h"

Camera::Camera() : _pos(sf::Vector2f(0.f,0.f)), _zoom(1.f)
{
}

sf::Vector2f Camera::getPos()
{
    return _pos;
}

float Camera::getZoom()
{
    return _zoom;
}

void Camera::setPos(sf::Vector2f new_pos)
{
    _pos = new_pos;
}

void Camera::zoomIn()
{
    _zoom *= 2.f;
}

void Camera::zoomOut()
{
    _zoom *= 0.5f;
}